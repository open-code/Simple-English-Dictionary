package com.zuhaibahmad.simpleenglishdictionary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Zuhaib on 12/31/2015.
 */
public class DictionaryDatabase {

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private DatabaseHelper mDBHelper;
    private SQLiteDatabase mDatabase;
    private Context context;


    //============================================================================================//
    //                                  Database Specific Variables                               //
    //============================================================================================//
    //Database Version
    private static final int DATABASE_VERSION = 1;
    //Database Name
    private static final String DATABASE_NAME = "DICTIONARY_DATABASE";

    //Table Names
    public static final String TABLE_ITEMS = "items";

    // Common column names
    private static final String KEY_ID = "id";
    private static final String KEY_WORD = "word";
    private static final String KEY_DEFINITION = "definition";

    // Create Bids table
    String CREATE_TABLE_SQL = "CREATE TABLE " + TABLE_ITEMS + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            + KEY_WORD + " TEXT,"
            + KEY_DEFINITION + " TEXT"
            + ")";

    //Get all items from Items table
    String GET_ALL_SAVED_ITEMS_SQL = "SELECT DISTINCT * FROM " + TABLE_ITEMS;


    //============================================================================================//
    //                                  Database Helper Class                                     //
    //============================================================================================//
    private class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            //Create User, Item and Bid tables
            db.execSQL(CREATE_TABLE_SQL);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);
            onCreate(db);
        }
    }

    // Database Constructor
    public DictionaryDatabase(Context c) {
        this.context = c;
    }

    // Open Database in Writable mode
    public DictionaryDatabase open() throws SQLException {
        mDBHelper = new DatabaseHelper(this.context);
        mDatabase = mDBHelper.getWritableDatabase();
        return this;
    }

    // Close Database
    public void close() {
        if (mDBHelper != null) {
            mDBHelper.close();
        }
    }

    //============================================================================================//
    //                              User Created Database Operations                              //
    //============================================================================================//

    // Get all auction items from database
    public ArrayList<Item> getAllItems() {
        ArrayList<Item> allAuctionItems = new ArrayList<>();
        Cursor c = mDatabase.rawQuery(GET_ALL_SAVED_ITEMS_SQL, null);
        c.moveToFirst();
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            allAuctionItems.add(new Item(
                            c.getInt(c.getColumnIndex(KEY_ID)),
                            c.getString(c.getColumnIndex(KEY_DEFINITION)),
                            c.getString(c.getColumnIndex(KEY_WORD))
                    )
            );
        }
        return allAuctionItems;
    }

    // Insert new Auction item to database
    public boolean addNewItem(String word, String definition) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_WORD, word);
        cv.put(KEY_DEFINITION, definition);
        long success = mDatabase.insert(TABLE_ITEMS, null, cv);
        if (success != -1) return true;
        return false;
    }
}

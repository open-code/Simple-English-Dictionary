package com.zuhaibahmad.simpleenglishdictionary;

/**
 * Created by Zuhaib on 1/20/2016.
 */
public class Item {
    private String word, definition;
    private int id;

    public Item(int id, String definition, String word) {
        this.id = id;
        this.definition = definition;
        this.word = word;
    }

    public int getId() {
        return id;
    }

    public String getWord() {
        return word;
    }

    public String getDefinition() {
        return definition;
    }
}

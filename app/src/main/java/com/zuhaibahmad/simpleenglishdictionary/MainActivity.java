package com.zuhaibahmad.simpleenglishdictionary;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "SimpleEnglishDictionary";
    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private LinearLayout logoLinearLayout, container;
    private EditText searchEditText;
    private ProgressBar progressbar;
    private ImageView submitButton;
    private RecyclerView definitionsRecyclerView;

    //============================================================================================//
    //                                  Reference Type Variables                                  //
    //============================================================================================//
    private RequestQueue queue;
    private AdView topAdView, bottomAdView;
    private DictionaryDatabase database;

    //============================================================================================//
    //                                  Reference Type Variables                                  //
    //============================================================================================//
    private String searchTerm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupLayout();
        database = new DictionaryDatabase(this);
    }

    private void setupLayout() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.MISave);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SavesActivity.class));
            }
        });

        container = (LinearLayout) findViewById(R.id.container);
        definitionsRecyclerView = (RecyclerView) findViewById(R.id.RVDefinitions);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        definitionsRecyclerView.setLayoutManager(llm);
        progressbar = (ProgressBar) findViewById(R.id.progressBar);
        logoLinearLayout = (LinearLayout) findViewById(R.id.logo);
        searchEditText = (EditText) findViewById(R.id.ETSearch);
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() <= 0) {
                    hideRecyclerView();
                }
            }
        });
        submitButton = (ImageView) findViewById(R.id.BTSubmit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchTerm = searchEditText.getText().toString();
                if (searchTerm != null && !searchTerm.isEmpty()) {
                    hideLogoAndShowAdview();
                    getDefinitions(searchTerm);
                } else {
                    searchEditText.setError("Please Submit A Valid Search Term");
                }
            }
        });

        topAdView = (AdView) findViewById(R.id.AVTop);
        bottomAdView = (AdView) findViewById(R.id.AVBottom);
        AdRequest adRequest = new AdRequest.Builder().build();
        topAdView.loadAd(adRequest);
        bottomAdView.loadAd(adRequest);
    }

    private void getDefinitions(String searchTerm) {
        String URL = "https://montanaflynn-dictionary.p.mashape.com/define?word=" + searchTerm.toLowerCase();

        showProgressbar();

        if (Utils.isNetworkOnline(this)) {
            StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    definitionsRecyclerView.setAdapter(new DefinitionsAdapter(parseDefinitions(s)));
                    hideProgressbar();
                    showRecyclerView();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    hideProgressbar();
                    Utils.showAlertDialogWithoutCancel(MainActivity.this, "Error", volleyError.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Mashape-Key", "BHdw0vU9e2mshTowJ6hFfZPqgqEap13j4PpjsnGCE0usFj9h76");
                    return params;
                }
            };
            if (queue == null) queue = Volley.newRequestQueue(this);
            queue.add(request);
        } else {
            hideProgressbar();
            Utils.showAlertDialogWithoutCancel(this, "Connection Error", "Network Not Found! Please Connect To A Network And Try Again");
        }
    }

    private void hideLogoAndShowAdview() {
        logoLinearLayout.setVisibility(View.GONE);
    }

    private void hideRecyclerView() {
        definitionsRecyclerView.animate().alpha(0.0f).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                definitionsRecyclerView.setVisibility(View.GONE);
            }
        });
    }

    private void showRecyclerView() {
        Log.e(TAG, "Showing!");
        definitionsRecyclerView.animate().alpha(100.0f).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                definitionsRecyclerView.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showProgressbar() {
        submitButton.setVisibility(View.GONE);
        progressbar.setVisibility(View.VISIBLE);
    }

    private void hideProgressbar() {
        submitButton.setVisibility(View.VISIBLE);
        progressbar.setVisibility(View.GONE);
    }

    private ArrayList<String> parseDefinitions(String s) {
        ArrayList<String> definitions = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(s);
            JSONArray array = object.getJSONArray("definitions");
            for (int i = 0; i < array.length(); i++) {
                String text = array.getJSONObject(i).getString("text");
                definitions.add(text);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return definitions;
    }

    private void save(String searchTerm, String definition) {
        boolean success = false;
        try {
            database.open();
            success = database.addNewItem(searchTerm, definition);
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Utils.showAlertDialogWithoutCancel(this, "Error", "Error Occurred While Accessing Database");
        }

        if(success)
            Utils.showLightSnackbar(container, "Definition Saved!");
    }

    /**
     * Called when leaving the activity
     */
    @Override
    public void onPause() {
        if (topAdView != null) {
            topAdView.pause();
        }
        if (bottomAdView != null) {
            bottomAdView.pause();
        }
        super.onPause();
    }

    /**
     * Called when returning to the activity
     */
    @Override
    public void onResume() {
        super.onResume();
        if (topAdView != null) {
            topAdView.resume();
        }
        if (bottomAdView != null) {
            bottomAdView.resume();
        }
    }

    /**
     * Called before the activity is destroyed
     */
    @Override
    public void onDestroy() {
        if (topAdView != null) {
            topAdView.destroy();
        }
        if (bottomAdView != null) {
            bottomAdView.destroy();
        }
        super.onDestroy();
    }

    public class DefinitionsAdapter extends RecyclerView.Adapter<DefinitionsAdapter.DefinitionItemViewHolder> {

        private List<String> items;

        public DefinitionsAdapter(List<String> items) {
            this.items = items;
        }

        @Override
        public DefinitionItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //Inflate layout
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.definition_item, parent, false);
            return new DefinitionItemViewHolder(v);
        }

        @Override
        public void onBindViewHolder(DefinitionItemViewHolder holder, int position) {
            String definition = items.get(position);
            Log.e(TAG, definition);
            holder.definitionTextView.setText(definition);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class DefinitionItemViewHolder extends RecyclerView.ViewHolder {

            TextView definitionTextView;
            ImageView saveButton;

            public DefinitionItemViewHolder(View itemView) {
                super(itemView);
                definitionTextView = (TextView) itemView.findViewById(R.id.TVDefinition);
                saveButton = (ImageView) itemView.findViewById(R.id.BTSave);
                saveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        save(searchTerm, definitionTextView.getText().toString());
                    }
                });
            }
        }
    }
}

package com.zuhaibahmad.simpleenglishdictionary;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.sql.SQLException;
import java.util.List;

public class SavesActivity extends AppCompatActivity {

    private AdView bottomAdView;
    private RecyclerView definitionsRecyclerView;
    private List<Item> itemsFromDatabase;
    private DictionaryDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saves);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        database = new DictionaryDatabase(this);

        bottomAdView = (AdView) findViewById(R.id.AVBottom);
        definitionsRecyclerView = (RecyclerView) findViewById(R.id.RVDefinitions);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        definitionsRecyclerView.setLayoutManager(llm);

        bottomAdView = (AdView) findViewById(R.id.AVBottom);
        AdRequest adRequest = new AdRequest.Builder().build();
        bottomAdView.loadAd(adRequest);

        definitionsRecyclerView.setAdapter(new DefinitionsAdapter(getItemsFromDatabase()));
    }

    public List<Item> getItemsFromDatabase() {
        try {
            database.open();
            itemsFromDatabase = database.getAllItems();
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Utils.showAlertDialogWithoutCancel(this, "Error", "Error Occurred While Accessing Database");
        }
        return itemsFromDatabase;
    }


    public class DefinitionsAdapter extends RecyclerView.Adapter<DefinitionsAdapter.DefinitionItemViewHolder> {

        private List<Item> items;

        public DefinitionsAdapter(List<Item> items) {
            this.items = items;
        }

        @Override
        public DefinitionItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //Inflate layout
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.save_item, parent, false);
            return new DefinitionItemViewHolder(v);
        }

        @Override
        public void onBindViewHolder(DefinitionItemViewHolder holder, int position) {
            Item item = items.get(position);
            holder.wordTextView.setText(item.getWord());
            holder.definitionTextView.setText(item.getDefinition());
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class DefinitionItemViewHolder extends RecyclerView.ViewHolder {

            TextView wordTextView, definitionTextView;

            public DefinitionItemViewHolder(View itemView) {
                super(itemView);
                definitionTextView = (TextView) itemView.findViewById(R.id.TVDefinition);
                wordTextView = (TextView) itemView.findViewById(R.id.TVWord);
            }
        }
    }
}

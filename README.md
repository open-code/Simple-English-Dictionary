Simple English Dictionary is a simple and elegant app that helps you search for 
definitions and save them for future viewing on the go!

Features:
- Instant Definations
- Copy to clipboard for sharing
- Save as favorite
- Favorite definition